;;; pm-read-xml.el --- Parse XML file -*- lexical-binding: t -*-
;;; Commentary:
;;; Parse XML file
;;; Code:
(require 'cl-lib)
(require 'esxml)

(defun pm/read-xml (file)
  "Read XML file FILE."
  (with-temp-buffer
    (insert-file-contents file)
    (xml-remove-comments (point-min) (point-max))
    (libxml-parse-xml-region (point-min) (point-max))))

(message (format "%S" (pm/read-xml "/Users/pmandal/programs/emacs-lisp/test.xml")))

(message (format "%s" (esxml-to-xml (pm/read-xml "/Users/pmandal/programs/emacs-lisp/test.xml"))))

(provide 'pm-read-xml)
;;; pm-read-xml.el ends here
