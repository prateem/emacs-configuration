;;; pm-shell-rest-snippet.el --- Shell and REST call snippets -*- lexical-binding: t -*-
;;; Commentary:
;;; Shell and REST call snippets
;;; Code:
(require 'request)

(defun run-cmd (cmd &rest args)
  "Run CMD with ARGS using `shell-file-name' as shell program.
Returns a pair of return value and string output."
  (with-temp-buffer
    (let ((retval (apply #'call-process-shell-command cmd nil t t args))
	  (prnout (buffer-string)))
      (cons retval prnout))))

(run-cmd "ls" "-a")

;; (request "http://httpbin.org/get"
;;   :params '(("key1" . "value1")
;; 	    ("key2" . "value2"))
;;   :parser #'json-read
;;   :)

(provide 'pm-shell-rest-snippet)
;;; pm-shell-rest-snippet.el ends here
