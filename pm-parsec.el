;;; pm-parsec.el --- Test parsec parser combinator library -*- lexical-binding: t -*-

;; Author: Prateem Mandal
;; Maintainer: Prateem Mandal
;; Version: 0.0.1
;; Package-Requires: (parsec)
;; Homepage: homepage
;; Keywords: keywords

;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Test parsec, a parser combinator library

;;; Code:

(require 'parsec)

(parsec-with-input "aaabaa3.1415-\n"
  (parsec-many (parsec-any-ch)))

(parsec-with-input "3.14159abc"
  (parsec-num 3.1415))

(parsec-with-input "abcx"
  (parsec-or (parsec-str "abde")
	     (parsec-str "abce")))

(provide 'pm-parsec)

;;; pm-parsec.el ends here
